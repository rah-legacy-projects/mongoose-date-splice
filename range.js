var moment = require('moment'),
    _ = require('lodash'),
    async = require('async'),
    mongoose = require('mongoose'),
    util = require('util'),
    logger = require('winston');
require('twix');

module.exports = exports = {
    plugin: function(schema, options) {
        schema.add({
            startDate: mongoose.Schema.Types.Mixed
        });
        schema.add({
            endDate: mongoose.Schema.Types.Mixed
        });
        schema.add({
            rangeActive: {
                type: Boolean,
                default: true
            }
        });
    },
    splice: function(unfilteredParts, newPart, cb) {
        //check that part has start, end and active
        if (!newPart.toObject()
            .hasOwnProperty('startDate') ||
            !newPart.toObject()
            .hasOwnProperty('endDate') ||
            !newPart.toObject()
            .hasOwnProperty('rangeActive')) {
            cb('new part must have start date, end date and range active defined', null);
            return;
        }
        //verify all parts have start,end and active
        if (_.filter(unfilteredParts, function(part) {
                return !part.toObject()
                    .hasOwnProperty('startDate') ||
                    !part.toObject()
                    .hasOwnProperty('endDate') ||
                    !part.toObject()
                    .hasOwnProperty('rangeActive');
            })
            .length > 0) {
            cb('part collection members must have start date, end date and range actie defined', null);
            return;
        }

        var partsToSave = [];

        _.chain(unfilteredParts)
            .filter(function(unfilteredPart) {
                return unfilteredPart.rangeActive;
            })
            .each(function(part) {
                var engulfed = false,
                    startOverlap = false,
                    endOverlap = false,
                    holepunch = false,
                    endForever = false,
                    startForever = false,
                    foreverEngulfed = false,
                    holepunchForever = false;

                //forever overlaps end side
                endForever = /forever/i.test(newPart.startDate) && (
                    /forever/i.test(part.startDate) ||
                    moment(part.startDate)
                    .isBefore(moment(newPart.endDate))
                );

                //forever overlaps start side
                startForever = /forever/i.test(newPart.endDate) && (
                    /forever/i.test(part.endDate) ||
                    moment(part.endDate)
                    .isAfter(moment(newPart.startDate))
                );

                //forever engulfs everything
                foreverEngulfed =
                //totally engulfed
                (/forever/i.test(newPart.endDate) && /forever/i.test(newPart.startDate)) ||
                //engulfed, before side touching
                (moment(part.startDate)
                    .isSame(moment(newPart.startDate)) && /forever/i.test(newPart.endDate)) ||
                //engulfed, end side touching
                (/forever/i.test(newPart.startDate) &&
                    moment(part.endDate)
                    .isSame(moment(newPart.endDate))) ||
                //engulfed, before side not touching
                (moment(part.startDate)
                    .isAfter(moment(newPart.startDate)) && /forever/i.test(newPart.endDate)) ||
                //engulfed, end side not touching
                (/forever/i.test(newPart.startDate) &&
                    moment(part.endDate)
                    .isBefore(moment(newPart.endDate)));

                //existing part has one/both forever, new part punches a hole
                holepunchForever = ((/forever/i.test(part.startDate) && !(/forever/i.test(newPart.startDate))) ||
                    moment(part.startDate)
                    .isBefore(newPart.startDate)) &&
                    ((/forever/i.test(part.endDate) && !(/forever/i.test(newPart.endDate))) ||
                    moment(part.endDate)
                    .isAfter(newPart.endDate))


                if (!(endForever || startForever || foreverEngulfed)) {
                    //engulfed by the new
                    engulfed =
                    //totally engulfed, no touching
                    ((moment(part.startDate)
                        .isAfter(moment(newPart.startDate)) && moment(part.endDate)
                        .isBefore(moment(newPart.endDate)))) ||

                    //totally engulfed, both touching
                    (moment(part.startDate)
                        .isSame(moment(newPart.startDate)) && moment(part.endDate)
                        .isSame(moment(newPart.endDate))) ||

                    //engulfed, before side touching
                    (moment(part.startDate)
                        .isSame(moment(newPart.startDate)) && moment(part.endDate)
                        .isBefore(moment(newPart.endDate))) ||

                    //engulfed, end side touching
                    (moment(newPart.startDate)
                        .isBefore(moment(part.startDate)) && moment(part.endDate)
                        .isSame(moment(newPart.endDate)));


                    //overlaps on the start side
                    startOverlap = moment(part.endDate)
                        .isAfter(moment(newPart.startDate)) && moment(part.startDate)
                        .isBefore(moment(newPart.startDate));
                    //overlaps on the end side
                    endOverlap = moment(part.startDate)
                        .isBefore(moment(newPart.endDate)) && moment(part.endDate)
                        .isAfter(moment(newPart.endDate));

                    //new engulfed by part
                    holepunch = startOverlap && endOverlap;
                }

                if (engulfed || foreverEngulfed) {
                    //if the record is engulfed, mark it as inactive
                    part.rangeActive = false;
                    partsToSave.push(part);
                } else if (holepunch || holepunchForever) {
                    //make three new records
                    //hack: use clone and delete the _id to make a mongoose copy
                    var left = _.clone(part.toObject());
                    delete left._id;
                    right = _.clone(left);
                    left = new part.constructor(left);
                    right = new part.constructor(right);

                    //part start to new part start less one day
                    left.startDate = part.startDate;
                    left.endDate = moment(newPart.startDate)
                        .add(-1, 'days')
                        .toDate();

                    //new part start to new part end
                    //the passed in new part will suffice

                    //new part end plus one day to part end
                    right.startDate = moment(newPart.endDate)
                        .add(1, 'days')
                        .toDate();
                    right.endDate = part.endDate;

                    //set part as inactive
                    part.rangeActive = false;

                    //add save items
                    partsToSave.push(left);
                    partsToSave.push(part);
                    partsToSave.push(right);
                } else if (startOverlap || startForever) {
                    //new part' s start is overlapped.

                    //make a clone of the unwrapped mongoose object
                    var activePart = _.clone(part.toObject());
                    //scorch the id so it's treated as new
                    delete activePart._id;
                    //use the part's constructor to create a new active part
                    activePart = new part.constructor(activePart);

                    //active part: part start to new part less one day
                    activePart.startDate = part.startDate;
                    activePart.endDate = moment(newPart.startDate)
                        .add(-1, 'days')
                        .toDate();

                    //inactivate part
                    part.rangeActive = false;

                    //add save items
                    partsToSave.push(activePart);
                    partsToSave.push(part);

                } else if (endOverlap || endForever) {
                    //new part's end is overlapped.
                    //make a clone of the unwrapped mongoose object
                    var activePart = _.clone(part.toObject());
                    //scorch the id so it's treated as new
                    delete activePart._id;
                    //use the part's constructor to create a new active part
                    activePart = new part.constructor(activePart);

                    //active part: new part end plus one day to part end
                    activePart.startDate = moment(newPart.endDate)
                        .add(1, 'days')
                        .toDate();
                    activePart.endDate = part.endDate;
                    //inactive part
                    part.rangeActive = false;
                    //add save items
                    partsToSave.push(activePart);
                    partsToSave.push(part);
                } else {
                    logger.silly('no criteria?');
                    logger.silly('hpforever: ' + holepunchForever);
                }
            })
            .value();
        //add the new part
        partsToSave.push(newPart);
        cb(null, partsToSave);
    },
    getByDateRange: function(object, rangeName, startDate, endDate, cb) {
        //flat should now contain all current data
        cb(null, module.exports.getByDateRangeSync(object, rangeName, startDate, endDate));
    },
    getCurrent: function(object, rangeName, cb) {
        module.exports.getByDateRange(object, rangeName, moment(), moment(), cb);
    },
    getByDateRangeSync: function(object, rangeName, startDate, endDate) {
        //todo: make sure object has rangename
        //nb: object must be lean
        //todo: check for leanness of object

        //filter to current ranges
        var currentRanges = _.filter(object[rangeName], function(objectRange) {
            if (!objectRange.rangeActive) {
                return false;
            }

            if (/forever/i.test(startDate) && /forever/i.test(endDate)) {
                return true;
            }
            if (/forever/i.test(startDate) && (moment(endDate)
                .isBefore(moment(objectRange.endDate)) || moment(endDate)
                .isSame(moment(objectRange.endDate)))) {
                return true;
            }
            if (/forever/i.test(endDate) && (moment(startDate)
                .isAfter(moment(objectRange.startDate)) || moment(startDate)
                .isSame(moment(objectRange.startDate)))) {
                return true;
            }

            var objStart = moment(/forever/i.test(objectRange.startDate) ? moment(startDate)
                    .subtract(1, 'seconds') : objectRange.startDate),
                objEnd = moment(/forever/i.test(objectRange.endDate) ? moment(endDate)
                    .add(1, 'seconds') : objectRange.endDate),
                rangeStart = moment(startDate),
                rangeEnd = moment(endDate);

            objRange = objStart.twix(objEnd);
            rangeRange = rangeStart.twix(rangeEnd);

            //dates are the same,
            var retval = (objStart.isSame(rangeStart) && objEnd.isSame(rangeEnd)) ||
                objRange.overlaps(rangeRange);
            return retval;
        });


        var flat = _.clone(object);
        delete flat[rangeName];

        if (currentRanges.length == 0) {
            return null;
        }

        //use the topmost range
        //clone the topmost range
        //remove the identifier as not to stomp the parent record's ID
        var currentRange = _.clone(currentRanges[0]);
        var rangeId = currentRange._id,
            rangeType = currentRange.__t;
        delete currentRange._id;
        delete currentRange.__t;
        if (currentRanges.length > 1) {
            logger.warn('object has multiple current ranges, using the first');
        }

        //add the range ID back into the flat
        flat._rangeId = rangeId;
        //add the range type back into the flat
        flat._rangeType = rangeType;

        //merge that data into the flattened object
        _.merge(flat, currentRange);

        //flat should now contain all current data
        return flat;
    },
    getCurrentSync: function(object, rangeName) {
        return module.exports.getByDateRangeSync(object, rangeName, moment(), moment());
    },
    dateSpliceSave: function(who, object, rangesToSave, rangeName, cb) {
        //nb: object must be mongoose object
        //todo: ensure object and ranges are mongoose objects
        //todo: ensure object[rangeName] exists
        //save the changes
        var saveTasks = [];
        _.each(rangesToSave, function(rangeToSave) {
            saveTasks.push(function(cb) {
                if (!rangeToSave._id || !rangeToSave.createdBy) {
                    rangeToSave.createdBy = who;
                }
                rangeToSave.modifiedBy = who;
                rangeToSave.save(function(err, result) {
                    if (!!err) {
                        logger.error('Problem in datesplice save range: ' + util.inspect(err));
                    }
                    cb(err, result);
                });
            });
        });
        async.parallel(saveTasks, function(err, r) {
            if (!!err) {
                logger.error(err);
            }
            //reconcile the ranged data with saved ranges
            object[rangeName] = _.filter(object[rangeName], function(rangedData) {
                return _.filter(rangesToSave, function(saveRangedData) {
                        return rangedData.id === saveRangedData.id;
                    })
                    .length == 0;
            });

            object[rangeName] = object[rangeName].concat(r);
            object.modifiedBy = who;
            object.save(function(err, savedObject) {
                if (!!err) {
                    logger.error('Problem in datesplice save container: ' + util.inspect(err));
                }
                cb(err, savedObject);
            });
        });
    }
};
