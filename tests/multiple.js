var mongoose = require('mongoose'),
    moment = require('moment'),
    dateSplice = require('../index')
    .splice,
    util = require('util'),
    async = require('async'),
    logger = require('winston'),
    SimpleModel = require('./models/simple'),
    testUtility = require('./util'),
    _ = require('lodash');




module.exports = exports = {
    setUp: function(cb) {
        mongoose.connect('mongodb://localhost/splice-sandbox');
        SimpleModel.remove(function(err) {
            cb();
        });
    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();
    },
    'no overlap': function(test) {
        testUtility.maker(
            'no overlap', [{
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('4-1-2014').toDate(),
                endDate: moment('4-30-2014').toDate()
            }], {
                startDate: moment('5-1-2014').toDate(),
                endDate: moment('5-15-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 1);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });
    },
    'start overlap with engulf': function(test) {
        testUtility.maker(
            'start overlap with engulf', [{
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('4-1-2014').toDate(),
                endDate: moment('4-30-2014').toDate()
            }], {
                startDate: moment('3-14-2014').toDate(),
                endDate: moment('5-15-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });

    },
    'end overlap with engulf': function(test) {
        testUtility.maker(
            'end overlap with engulf', [{
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('4-1-2014').toDate(),
                endDate: moment('4-30-2014').toDate()
            }], {
                startDate: moment('2-14-2014').toDate(),
                endDate: moment('4-15-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });

    },
    'both overlap with engulf': function(test) {
        testUtility.maker(
            'both overlap with engulf', [{
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('4-1-2014').toDate(),
                endDate: moment('4-30-2014').toDate()
            }, {
                startDate: moment('5-1-2014').toDate(),
                endDate: moment('5-31-2014').toDate()
            }], {
                startDate: moment('3-14-2014').toDate(),
                endDate: moment('5-15-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 6);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 3);
                test.done();
            });

    },
    'engulf with no touching': function(test) {
        testUtility.maker(
            'engulf with no touching', [{
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('4-1-2014').toDate(),
                endDate: moment('4-30-2014').toDate()
            }, {
                startDate: moment('5-1-2014').toDate(),
                endDate: moment('5-31-2014').toDate()
            }], {
                startDate: moment('2-14-2014').toDate(),
                endDate: moment('6-15-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },
'engulf with before end touching': function(test) {
        testUtility.maker(
            'engulf with before end touching', [{
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('4-1-2014').toDate(),
                endDate: moment('4-30-2014').toDate()
            }, {
                startDate: moment('5-1-2014').toDate(),
                endDate: moment('5-31-2014').toDate()
            }], {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('6-31-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },
'engulf with after end touching': function(test) {
        testUtility.maker(
            'engulf with after end touching', [{
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('4-1-2014').toDate(),
                endDate: moment('4-30-2014').toDate()
            }, {
                startDate: moment('5-1-2014').toDate(),
                endDate: moment('5-31-2014').toDate()
            }], {
                startDate: moment('2-1-2014').toDate(),
                endDate: moment('5-31-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },
    'engulf with both ends touching': function(test) {
        testUtility.maker(
            'engulf with both ends touching', [{
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('4-1-2014').toDate(),
                endDate: moment('4-30-2014').toDate()
            }, {
                startDate: moment('5-1-2014').toDate(),
                endDate: moment('5-31-2014').toDate()
            }], {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('5-31-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },
    'multiple records, same date': function(test) {
        testUtility.maker(
            'multiple records, same date', [{
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }], {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },

};
