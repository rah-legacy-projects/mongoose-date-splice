var mongoose = require('mongoose'),
    moment = require('moment'),
    dateSplice = require('../../index')
    .splice,
    util = require('util'),
    async = require('async'),
    logger = require('winston'),
    testUtility = require('../util'),
    SimpleModel = require('../models/simple'),
    _ = require('lodash');


module.exports = exports = {
    setUp: function(cb) {
        mongoose.connect('mongodb://localhost/splice-sandbox');
        SimpleModel.remove(function(err) {
            cb();
        });
    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();
    },
    'no overlap': function(test) {
        testUtility.maker(
            'no overlap', [{
                startDate: 'forever',
                endDate: moment('3-31-2014')
            }, {
                startDate: moment('4-1-2014'),
                endDate: moment('4-30-2014')
            }], {
                startDate: moment('5-1-2014'),
                endDate: 'forever'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 1);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });
    },
    'start overlap with engulf': function(test) {
        testUtility.maker(
            'start overlap with engulf', [{
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('4-1-2014').toDate(),
                endDate: moment('4-30-2014').toDate()
            }], {
                startDate: moment('3-14-2014').toDate(),
                endDate: 'forever'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });

    },
    'end overlap with engulf': function(test) {
        testUtility.maker(
            'end overlap with engulf', [{
                startDate: moment('3-1-2014'),
                endDate: moment('3-31-2014')
            }, {
                startDate: moment('4-1-2014'),
                endDate: moment('4-30-2014')
            }], {
                startDate: 'forever',
                endDate: moment('4-15-2014')
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });

    },
    'engulf': function(test) {
        testUtility.maker(
            'engulf with no touching', [{
                startDate: moment('3-1-2014'),
                endDate: moment('3-31-2014')
            }, {
                startDate: moment('4-1-2014'),
                endDate: moment('4-30-2014')
            }, {
                startDate: moment('5-1-2014'),
                endDate: moment('5-31-2014')
            }], {
                startDate: 'forever',
                endDate: 'forever'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },
    'engulf with before end touching': function(test) {
        testUtility.maker(
            'engulf with before end touching', [{
                startDate: moment('3-1-2014'),
                endDate: moment('3-31-2014')
            }, {
                startDate: moment('4-1-2014'),
                endDate: moment('4-30-2014')
            }, {
                startDate: moment('5-1-2014'),
                endDate: moment('5-31-2014')
            }], {
                startDate: moment('3-1-2014'),
                endDate: 'forever'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },
    'engulf with after end touching': function(test) {
        testUtility.maker(
            'engulf with after end touching', [{
                startDate: moment('3-1-2014'),
                endDate: moment('3-31-2014')
            }, {
                startDate: moment('4-1-2014'),
                endDate: moment('4-30-2014')
            }, {
                startDate: moment('5-1-2014'),
                endDate: moment('5-31-2014')
            }], {
                startDate: 'forever',
                endDate: moment('5-31-2014')
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },
    'engulf with both ends touching': function(test) {
        testUtility.maker(
            'engulf with both ends touching', [{
                startDate: 'forever',
                endDate: moment('3-31-2014')
            }, {
                startDate: moment('4-1-2014'),
                endDate: moment('4-30-2014')
            }, {
                startDate: moment('5-1-2014'),
                endDate: 'forever'
            }], {
                startDate: 'forever',
                endDate: 'forever'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },
    'multiple records, all forever': function(test) {
        testUtility.maker(
            'multiple records, same date', [{
                startDate: 'forever',
                endDate: 'forever'
            }, {
                startDate: 'forever',
                endDate: 'forever'
            }, {
                startDate: 'forever',
                endDate: 'forever'
            }], {
                startDate: 'forever',
                endDate: 'forever'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },
};
