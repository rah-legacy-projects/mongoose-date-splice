var mongoose = require('mongoose'),
    moment = require('moment'),
    ix = require('../../index'),
    util = require('util'),
    async = require('async'),
    testUtility = require('../util'),
    SimpleModel = require('../models/simple'),
    logger = require('winston'),
    _ = require('lodash');


module.exports = exports = {
    setUp: function(cb) {
        mongoose.connect('mongodb://localhost/splice-sandbox');
        SimpleModel.remove(function(err) {
            logger.silly('setup complete.');
            cb();
        });
    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();
    },
    'no overlap': function(test) {
        testUtility.maker(
            'start overlap', {
                startDate: moment('3-1-2014')
                    .toDate(),
                endDate: moment('3-31-2014')
                    .toDate()
            }, {
                startDate: moment('4-1-2014')
                    .toDate(),
                endDate: 'forever'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 1);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });
    },
    'start overlap': function(test) {
        testUtility.maker(
            'start overlap', {
                startDate: '3-1-2014',
                endDate: '3-31-2014'
            }, {
                startDate: '3-15-2014',
                endDate: 'forever'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 3);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });
    },
    'start overlap end touch': function(test) {
        testUtility.maker(
            'start overlap end touch', {
                startDate: '3-1-2014',
                endDate: '3-31-2014'
            }, {
                startDate: '3-15-2014',
                endDate: 'forever'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 3);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });
    },
    'end overlap': function(test) {
        testUtility.maker(
            'end overlap', {
                startDate: '3-1-2014',
                endDate: '3-31-2014'
            }, {
                startDate: 'forever',
                endDate: '3-15-2014'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 3);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });

    },
    'end overlap start touch': function(test) {
        testUtility.maker(
            'end overlap', {
                startDate: '3-1-2014',
                endDate: '3-31-2014'
            }, {
                startDate: 'forever',
                endDate: '3-15-2014'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 3);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });

    },
    engulf: function(test) {
        testUtility.maker(
            'engulf', {
                startDate: '3-15-2014',
                endDate: '3-21-2014'
            }, {
                startDate: 'forever',
                endDate: 'forever'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 2);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },
    'no overlap, get by date': function(test) {
        testUtility.makerFlattener(
            'no overlap, get by date', {
                startDate: moment('3-1-2014')
                    .toDate(),
                endDate: moment('3-31-2014')
                    .toDate()
            }, {
                startDate: moment('4-1-2014')
                    .toDate(),
                endDate: 'forever'
            },
            '4-15-2014',
            function(err, flattened) {
                test.ifError(err);
                test.equal(flattened.endDate, 'forever');
                test.ok(moment(flattened.startDate)
                    .isSame(moment('4-1-2014')));
                test.done();
            });
    },
    'start overlap': function(test) {
        testUtility.makerFlattener(
            'start overlap', {
                startDate: '3-1-2014',
                endDate: '3-31-2014'
            }, {
                startDate: '3-15-2014',
                endDate: 'forever'
            },
            '4-15-2014',
            function(err, flattened) {
                test.ifError(err);
                test.equal(flattened.endDate, 'forever');
                test.ok(moment(flattened.startDate)
                    .isSame(moment('3-15-2014')));
                test.done();
            });
    },
    'start overlap end touch': function(test) {
        testUtility.makerFlattener(
            'start overlap end touch', {
                startDate: '3-1-2014',
                endDate: '3-31-2014'
            }, {
                startDate: '3-15-2014',
                endDate: 'forever'
            },
            '4-15-2014',
            function(err, flattened) {
                test.ifError(err);
                test.equal(flattened.endDate, 'forever');
                test.ok(moment(flattened.startDate)
                    .isSame(moment('3-15-2014')));
                test.done();
            });
    },
    'end overlap': function(test) {
        testUtility.makerFlattener(
            'end overlap', {
                startDate: '3-1-2014',
                endDate: '3-31-2014'
            }, {
                startDate: 'forever',
                endDate: '3-15-2014'
            },
            '1-15-2014',
            function(err, flattened) {
                test.ifError(err);
                test.equal(flattened.startDate, 'forever');
                test.ok(moment(flattened.endDate)
                    .isSame(moment('3-15-2014')));
                test.done();
            });

    },
    'end overlap start touch': function(test) {
        testUtility.makerFlattener(
            'end overlap', {
                startDate: '3-1-2014',
                endDate: '3-31-2014'
            }, {
                startDate: 'forever',
                endDate: '3-15-2014'
            },
            '1-15-2014',
            function(err, flattened) {
                test.ifError(err);
                test.equal(flattened.startDate, 'forever');
                test.ok(moment(flattened.endDate)
                    .isSame(moment('3-15-2014')));
                test.done();
            });

    },
    engulf: function(test) {
        testUtility.makerFlattener(
            'engulf', {
                startDate: '3-15-2014',
                endDate: '3-21-2014'
            }, {
                startDate: 'forever',
                endDate: 'forever'
            },
            '4-15-2014',
            function(err, flattened) {
                test.ifError(err);
                test.equal(flattened.endDate, 'forever');
                test.equal(flattened.startDate, 'forever');
                test.done();
            });

    },
    same: function(test) {
        testUtility.makerFlattener(
            'same', {
                startDate: '3-15-2014',
                endDate: 'forever'
            }, {
                startDate: '3-15-2014',
                endDate: 'forever'
            },
            '4-15-2014', function(err, flattened, unflattened) {
                test.ifError(err);
                test.equal(flattened.endDate, 'forever');
                test.equal(flattened.startDate, '3-15-2014');
                test.done();
            });
    },
    flattenToEndForever: function(test) {
        testUtility.makerFlattener(
            'flattenToForever', {
                startDate: '3-15-2014',
                endDate: 'forever'
            }, {
                startDate: '3-15-2014',
                endDate: 'forever'
            },
            '4-15-2014', function(err, flattened, unflattened) {
                SimpleModel.find({
                    name: 'flattenToForever'
                }).lean()
                    .exec(function(err, simples) {
                        var container = {
                            rangedData: simples,
                            containerName: 'test'
                        };
                        var flattened = ix.getByDateRangeSync(container, 'rangedData', '4-15-2014', 'forever');
                        test.ifError(err);
                        test.equal(flattened.endDate, 'forever');
                        test.equal(flattened.startDate, '3-15-2014');
                        test.done();
                    });

            });

    },
    flattenToEndForeverTouch: function(test) {
        testUtility.makerFlattener(
            'flattenToForever', {
                startDate: '3-15-2014',
                endDate: 'forever'
            }, {
                startDate: '3-15-2014',
                endDate: 'forever'
            },
            '3-15-2014', function(err, flattened, unflattened) {
                SimpleModel.find({
                    name: 'flattenToForever'
                }).lean()
                    .exec(function(err, simples) {
                        var container = {
                            rangedData: simples,
                            containerName: 'test'
                        };
                        var flattened = ix.getByDateRangeSync(container, 'rangedData', '3-15-2014', 'forever');
                        test.ifError(err);
                        test.equal(flattened.endDate, 'forever');
                        test.equal(flattened.startDate, '3-15-2014');
                        test.done();
                    });

            });

    },
    flattenToStartForever: function(test) {
        testUtility.makerFlattener(
            'flattenToForever', {
                startDate: 'forever',
                endDate: '5-15-2014'
            }, {
                startDate: 'forever',
                endDate: '5-15-2014'
            },
            '4-15-2014', function(err, flattened, unflattened) {
                SimpleModel.find({
                    name: 'flattenToForever'
                }).lean()
                    .exec(function(err, simples) {
                        var container = {
                            rangedData: simples,
                            containerName: 'test'
                        };
                        var flattened = ix.getByDateRangeSync(container, 'rangedData', 'forever', '4-15-2014');
                        test.ifError(err);
                        test.equal(flattened.endDate, '5-15-2014');
                        test.equal(flattened.startDate, 'forever');
                        test.done();
                    });

            });

    },
    flattenToBothForever: function(test) {
        testUtility.makerFlattener(
            'flattenToForever', {
                startDate: '3-15-2014',
                endDate: 'forever'
            }, {
                startDate: '3-15-2014',
                endDate: 'forever'
            },
            '4-15-2014', function(err, flattened, unflattened) {
                SimpleModel.find({
                    name: 'flattenToForever'
                }).lean()
                    .exec(function(err, simples) {
                        var container = {
                            rangedData: simples,
                            containerName: 'test'
                        };
                        var flattened = ix.getByDateRangeSync(container, 'rangedData', 'forever', 'forever');
                        test.ifError(err);
                        test.equal(flattened.endDate, 'forever');
                        test.equal(flattened.startDate, '3-15-2014');
                        test.done();
                    });

            });

    },
    'holepunch in existing forever': function(test) {
        testUtility.maker(
            'sandbox', {
                startDate: 'forever',
                endDate: 'forever'
            }, {
                startDate: '3-15-2014',
                endDate: '4-15-2014'
            },
            function(err, savedParts) {
                logger.silly('saved parts: ' + util.inspect(savedParts));
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 3);
                test.done();
            });
    },
    'holepunch in existing forever, original end date fixed': function(test) {
        testUtility.maker(
            'sandbox', {
                startDate: 'forever',
                endDate: '5-15-2014'
            }, {
                startDate: '3-15-2014',
                endDate: '4-15-2014'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 3);
                test.done();
            });
    },
    'holepunch in existing forever, original start date fixed': function(test) {
        testUtility.maker(
            'sandbox', {
                startDate: '2-15-2014',
                endDate: 'forever'
            }, {
                startDate: '3-15-2014',
                endDate: '4-15-2014'
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 3);
                test.done();
            });
    },
};
