var mongoose = require('mongoose'),
    moment = require('moment'),
    util = require('util'),
    async = require('async'),
    testUtility = require('./util'),
    SimpleModel = require('./models/simple'),
    _ = require('lodash');


module.exports = exports = {
    setUp: function(cb) {
        mongoose.connect('mongodb://localhost/splice-sandbox');
        SimpleModel.remove(function(err) {
            cb();
        });
    },
    tearDown: function(cb) {
        mongoose.disconnect();
        cb();
    },
    'no overlap': function(test) {
        testUtility.maker(
            'start overlap', {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('4-1-2014').toDate(),
                endDate: moment('5-15-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 1);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });
    },
    'start overlap': function(test) {
        testUtility.maker(
            'start overlap', {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('3-15-2014').toDate(),
                endDate: moment('4-15-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 3);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });
    },
    'start overlap end touch': function(test) {
        testUtility.maker(
            'start overlap end touch', {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('3-15-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 3);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });
    },
    'end overlap': function(test) {
        testUtility.maker(
            'end overlap', {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('2-15-2014').toDate(),
                endDate: moment('3-15-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 3);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });

    },
    'end overlap start touch': function(test) {
        testUtility.maker(
            'end overlap', {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-15-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 3);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 2);
                test.done();
            });

    },

    engulf: function(test) {
        testUtility.maker(
            'engulf', {
                startDate: moment('3-15-2014').toDate(),
                endDate: moment('3-21-2014').toDate()
            }, {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 2);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });

    },
    holepunch: function(test) {
        testUtility.maker(
            'engulf', {
                startDate: moment('3-1-2014').toDate(),
                endDate: moment('3-31-2014').toDate()
            }, {
                startDate: moment('3-15-2014').toDate(),
                endDate: moment('3-21-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 4);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 3);
                test.done();
            });
    },

    'exact engulf': function(test) {
        testUtility.maker(
            'engulf', {
                startDate: moment('3-15-2014').toDate(),
                endDate: moment('3-21-2014').toDate()
            }, {
                startDate: moment('3-15-2014').toDate(),
                endDate: moment('3-21-2014').toDate()
            },
            function(err, savedParts) {
                test.ifError(err);
                test.equal(savedParts.length, 2);
                testUtility.continuity(savedParts, test);
                test.equal(_.filter(savedParts, function(part) {
                        return part.rangeActive;
                    })
                    .length, 1);
                test.done();
            });
    }


};
