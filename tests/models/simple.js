var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId

var simpleSchema = new Schema({
    name: {
        type: String,
        required: true
    },
}, {});

simpleSchema.plugin(require('../../range').plugin, 'Simple', {});
var simpleModel = mongoose.model('Simple', simpleSchema);
var simple = module.exports = exports = simpleModel;
