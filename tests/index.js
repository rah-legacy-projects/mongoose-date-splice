module.exports = exports = {
	forever: require("./forever"),
	multiple: require("./multiple"),
	simple: require("./simple"),
};
