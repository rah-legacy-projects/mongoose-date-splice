var logger = require('winston');

logger.remove(logger.transports.Console);
//if (/development/.test(process.env.ENV)) {
logger.add(logger.transports.Console, {
    level: 'fatal',
    colorize: true
});
logger.silly('added silly console');
//}


process.on('uncaughtException', function(err) {
    console.log('UNCAUGHT: ' + err);
    console.log('UNCAUGHT: ' + err.stack);
});
