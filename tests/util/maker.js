var _ = require('lodash'),
    moment = require('moment'),
    logger = require('winston'),
    util = require('util'),
    SimpleModel = require('../models/simple'),
    dateSplice = require('../../index').splice,
    async = require('async');
module.exports = exports = function(name, setOneArray, setTwo, cbl) {
    if(!_.isArray(setOneArray)){
        logger.warn('set one is not an array, coercing');
        setOneArray = [setOneArray];
    }

    var settasks = [];
    _.each(setOneArray, function(a) {
        settasks.push(function(cb) {
            (new SimpleModel({
                name: name,
                startDate: a.startDate,
                endDate: a.endDate
            }))
                .save(function(err, saved) {
                    cb(err, saved);
                });
        });
    });

    async.series(settasks, function(err, r) {
        //find start overlap items
        SimpleModel.find({
            name: name
        })
            .exec(function(err, items) {
                //make another item to splice
                var newItem = new SimpleModel({
                    name: name,
                    startDate: setTwo.startDate,
                    endDate: setTwo.endDate,
                });
                dateSplice(items, newItem, function(err, saveables) {
                    var savetasks = [];
                    _.each(saveables, function(s) {
                        savetasks.push(function(cb) {
                            s.save(function(err, saved) {
                                cb(err, saved);
                            });
                        });
                    });

                    async.parallel(savetasks, function(err, r) {
                        cbl(err, r);
                    });
                });

            });
    });

};
