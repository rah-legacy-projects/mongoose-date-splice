var _ = require('lodash'),
    moment = require('moment'),
    logger = require('winston'),
    util = require('util');
module.exports = exports = function(parts, test) {
    var filteredParts = _.filter(parts, function(part) {
        return part.rangeActive;
    });
    var sortedParts = _.sortBy(filteredParts, function(part) {
        if(/forever/i.test(part.startDate)){
            return new Date(-8640000000000000)
        }
        if(/forever/i.test(part.endDate)){
            new Date(8640000000000000)
        }
        return moment(part.startDate).toDate();
    });
    logger.silly('continuity parts: ' + util.inspect(sortedParts));
    var foreverStartFound = false,
        foreverEndFound = false;
    _.each(sortedParts, function(sortedPart, six) {
        foreverStartFound = foreverStartFound || /forever/i.test(sortedPart.startDate);
        foreverEndFound = foreverEndFound || /forever/i.test(sortedPart.endDate);
        if (!!sortedParts[six + 1]) {
            //great continuity! /thumbsup
            test.ok(
                moment(sortedParts[six].endDate)
                .isBefore(moment(sortedParts[six + 1].startDate)));
        }
    });
};
